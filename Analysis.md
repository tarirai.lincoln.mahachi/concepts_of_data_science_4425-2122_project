This file contains the analysis of time-complexity of the three methods.

The time complexity for the insertion operation in the AVL tree is O(log n), which involves searching the position to insert and also returning to the root. The same applies to the time complexity for the deletion operation of the AVL tree is also O(log n).
