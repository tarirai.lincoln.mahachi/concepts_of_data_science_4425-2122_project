# Concepts_Of_Data_Science_4425-2122_Project

This is a repository to manage our data and collaborate with each other. This gitlab repository will include the following:
A python class of a self-balancing AVL binary search tree .
Search for node, Insert node and Remove node methods all in the main.py file. The functionality of the program is contained in the README_DEV.md file. The analysis.md file, contains the analysis of time-complexity of the three methods mentioned above.

Two datafiles one with 20 numbers, one with more than 2 000 words that is the numbers.txt and words.txt respectively are also included. These were used to develop and to demonstrate our solutions. 

An AVL ( Adelson Velskii and Landis) tree is a binary search tree with a balance condition. The condition must be easy to maintain, and it ensure that the depth of the tree is Olog(log n). The basic idea is to require that left and right sub trees have the same height


Group members: 
Adina Asim
Bhanupriya Dixit
Tarirai Lincoln Mahachi
